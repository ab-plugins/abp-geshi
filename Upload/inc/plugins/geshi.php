<?php
/**
 * Geshi Mycode Plugin
 * Author: CrazyCat <crazycat@c-p-f.org>
 * Version 1.1
 *
 */

if(!defined("IN_MYBB"))
	die('Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.');

define('CN_ABPGESHI', str_replace('.php', '', basename(__FILE__)));

$plugins->add_hook("parse_message_start", "geshi_message_start");
$plugins->add_hook("parse_message_end", "geshi_message_end");

/**
 * Displayed informations
 */
function geshi_info()
{
	global $lang;
	$lang->load(CN_ABPGESHI);
	return array(
		'name'		=> $lang->geshi_name,
		'description'	=> $lang->geshi_desc,
		'website'	=> 'http://ab-plugin.cc/ABP-GeSHi-t-4.html',
		'author'	=> 'CrazyCat',
		'authorsite'	=> 'http://ab-plugins.cc',
		'version'	=> '1.2',
		'compatibility'	=> '18*',
		'codename'	=> CN_ABPGESHI
	);
}

function geshi_install()
{
	global $db, $lang;
	require_once(dirname(__FILE__).'/syntaxhl/geshi.php');
	$lang->load(CN_ABPGESHI);
	// Setting group
    $settinggroups = array(
        'name' => CN_ABPGESHI,
        'title' => $lang->geshi_setting_title,
        'description' => $lang->geshi_setting_description,
        'disporder' => 0,
        "isdefault" => 0
    );

    $db->insert_query('settinggroups', $settinggroups);
    $gid = $db->insert_id();

    $linenumopts = array(
		GESHI_NO_LINE_NUMBERS . '=' . $lang->geshi_nolinenum,
		GESHI_NORMAL_LINE_NUMBERS . '=' . $lang->geshi_linenum_normal,
		GESHI_FANCY_LINE_NUMBERS . '=' . $lang->geshi_linenum_fancy
	);
    $settings[] = array(
        'name' => CN_ABPGESHI . '_line_style',
        'title' => $lang->geshi_line_style_title,
        'description' => $lang->geshi_line_style_description,
        'optionscode' => 'select'.PHP_EOL.implode(PHP_EOL, $linenumopts),
        'value' => GESHI_NO_LINE_NUMBERS,
        'disporder' => 1
    );
	foreach ($settings as $i => $setting) {
        $insert = array(
            'name' => $db->escape_string($setting['name']),
            'title' => $db->escape_string($setting['title']),
            'description' => $db->escape_string($setting['description']),
            'optionscode' => $db->escape_string($setting['optionscode']),
            'value' => $db->escape_string($setting['value']),
            'disporder' => $setting['disporder'],
            'gid' => $gid,
        );
        $db->insert_query('settings', $insert);
    }
    rebuild_settings();
}

function geshi_uninstall()
{
	global $db;
    $db->delete_query('settings', "name LIKE '" . CN_ABPGESHI . "_%'");
    $db->delete_query('settinggroups', "name = '" . CN_ABPGESHI . "'");
    rebuild_settings();
    geshi_deactivate();
}

function geshi_is_installed()
{
	global $mybb;
    if (isset($mybb->settings[CN_ABPGESHI.'_line_style'])) {
        return true;
    }
    return false;
}

function geshi_activate()
{
	global $db;
	$template = array(
		'title' => CN_ABPGESHI.'_code',
		'template' => '<div class="codeblock"><div class="title">{$glang}</div><div class="body"><code>{$code}</code></div></div><br />',
		'sid' => -1,
        'version' => 1.0,
        'dateline' => TIME_NOW
	);
	$db->insert_query("templates", $template);
}

function geshi_deactivate()
{
    global $db;
    $db->delete_query('templates', "title LIKE '" . CN_ABPGESHI . "_%'");
}

function geshi_message_start($message)
{
	global $geshi_matches;
	preg_match_all("#\[geshi=(.*?)\](.*?)\[/geshi\](\r\n?|\n?)#si", $message, $geshi_matches, PREG_SET_ORDER);
	$message = preg_replace("#\[geshi=(.*?)\](.*?)\[/geshi\](\r\n?|\n?)#si", "<geshi-code>\n", $message);
	return $message;
}

function geshi_parse($str, $slang)
{
	global $mybb, $templates, $lang;
	require_once(dirname(__FILE__).'/syntaxhl/geshi.php');
	
	$str = preg_replace('#^(\t*)(\n|\r|\0|\x0B| )*#', '\\1', $str);
	$str = rtrim($str);

	$original = preg_replace('#^\t*#', '', $str);
	if(empty($original))
	{
		return;
	}
	$geshi = new GeSHi($str, $slang);
	
	if ($geshi->error() && $geshi->error == GESHI_ERROR_NO_SUCH_LANG)
	{
		$lang->load(CN_ABPGESHI);
		$suplanguages = $geshi->get_supported_languages();
		$geshi_code = '<strong>' . sprintf($lang->geshi_not_supported_language, $slang) . '</strong><br />' . PHP_EOL;
		$geshi_code .= $lang->geshi_list_available_languages . '<br />' . PHP_EOL;
		$geshi_code .= implode(', ', $suplanguages);
	}
	else
	{
		$glang = $geshi->get_language_name();
		$geshi->enable_line_numbers($mybb->settings[CN_ABPGESHI.'_line_style']);
		$code = $geshi->parse_code();
		$code = preg_replace('#<code>\s*<span style="color: \#000000">\s*#i', "<code>", $code);
		$code = preg_replace("#</span>\s*</code>#", "</code>", $code);
		$code = preg_replace("#</span>(\r\n?|\n?)</code>#", "</span></code>", $code);
		$code = str_replace("\\", '&#092;', $code);
		$code = str_replace('$', '&#36;', $code);
		$code = preg_replace("#&amp;\#([0-9]+);#si", "&#$1;", $code);

		$code = preg_replace("#<span style=\"color: \#([A-Z0-9]{6})\"></span>#", "", $code);
		$code = str_replace("<code>", "<div dir=\"ltr\"><code>", $code);
		$code = str_replace("</code>", "</code></div>", $code);
		$code = preg_replace("# *$#", "", $code);
		eval("\$geshi_code = \"".$templates->get(CN_ABPGESHI.'_code', 1, 0)."\";");
	}
	return $geshi_code;
}

function geshi_message_end($message)
{
	global $geshi_matches;
	//echo '<pre>', var_dump($geshi_matches), '</pre><br />';
//	echo '<pre>', var_dump($message), '</pre><br />';
	if(count($geshi_matches) > 0)
	{
		foreach($geshi_matches as $text)
		{
			$code = geshi_parse($text[2], $text[1]);
			$message = preg_replace("#&lt;geshi-code&gt;\n?#", $code, $message, 1);
		}
	}
	return $message;
}

