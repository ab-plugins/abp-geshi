# ABP GeSHi

This plugin is an implementation of GeSHi (Generic Syntax Highlighter - http://qbnz.com/highlighter/) for MyBB.

No changes have been made to GeSHi, so you can easily change the version.

# Usage
Use the MyCode `[geshi=lang]your code here[/geshi]` to have your code highlighted. If the language is not supported, a list of supported languages will be displayed.

# Installation / Upgrade
Upload the content of the UPLOAD directory to your forum root and activate the plugin.

# Settings
Actually, only one setting exists: the display of line numbers (default off).